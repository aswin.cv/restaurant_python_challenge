'''
This script will take the date and time from the user and  will return the names of the open  restaurants at given time
'''

'''
Use pandas to read the csv file
Use datetiem for date & time handling
Use re for regular expression

'''
import pandas as pd;
import datetime;
import re;
import sys;

'''
find_open_restaurants(file_name,date) this function returns the name of available restaurant at the given date  & time
'''
def find_open_restaurants(file_name, date):

    '''
        data is the variable which contains the csv data.

        pd is the alias name of the pandas module

        read_csv() is the pandas method to read csv data  &read_csv() method returns the pandaas dataframe
        and in read_csv()method we us "header=None"  as the given csv file does not contains the coloumn.so by default pandas will assume 1st row as header row.
        j
    '''
    data = pd.read_csv(file_name, header=None);

    '''
        names is list which inculude all the resturant names available at the given time and this variable is returned by the  this function
    '''
    names = [];

    '''
        week is a varibale which contains all the week days .
    '''
    week=["Mon","Tues","Wed","Thu","Fri","Sat","Sun"];

    '''
        This loop is iterating over the data read from the csv row wise. values is a  list which contains the coloumn values. coloumn 1 contain name &
        coloumn 2 contain restaurant info .so value contains this 2 coloumn value as list.

        iterrows() is the pandas function to iterate row wise over dataframe
    '''
    for key, values in data.iterrows():
        '''
            Here we split the restaurant information by delimeter "/"
             and details will contains the list of string seperated by delimeter "/"

             for eg
                values= "Mon-Thu, Sun 11:30 am - 9 pm   / Fri-Sat 11:30 am - 9:30 pm"
                than details will conatins 2 string
                details=["Mon-Thu, Sun 11:30 am - 9 pm"   ," Fri-Sat 11:30 am - 9:30 pm"]
        '''
        details = values[1].split("/");
        '''
        to get the day of the week for eg "Mon" if the day is monday on the date user give
        '''
        day=date.strftime("%a");
        '''
        to get the day number of the   week day
        '''
        day_no=date.weekday();
        #print(day);
        '''
        as  mentioned above details contains  the list of string.& here we are iterating over each string of details to check
        wether the day entered by the user is present or not.
        '''
        for each in details:
            '''
            day_found is varible which is use to check whethe the day is present in the string if it is present than we can check the
            time .if the day is not present than  we can check in next string.

            time_found is variable which is use to check whether the restaurant at the time enterd by user is available or not;

            when day_found & time_found both are 1 than that this string contains the available information and we can break the loop.
            no need to check further
            '''
            day_found=0;
            time_found=0;
            status = re.search(day, each);
            '''
                this below code is checking for the day present or not
            '''
            if status  is not None:
                    day_found=1;
            else:
                for temp in week:
                    for temp1 in week[day_no:]:
                        status = re.search(temp + "-" + temp1, each);
                        if status is not None:
                            day_found = 1;
                            break;
                    if day_found == 1:
                        break;
            '''
            here if day_found=1 than only we have to check for the time if day is not present in the string no way we get
            the required informaton.& we can brak the loop.continue for the next string
            '''
            if day_found==1:
                '''
                this below code is checking for the time
                search_time is a variable which is entered by the user.but we have to manipulate to 24hours format
                opening_time is a variable which is the opening time of restaurant.
                closing_time is a variable which is the closing time of restaurant.
                '''
                search_time=0.0;
                search_time=date.hour;
                search_time=str(search_time)+"."+str(date.minute);
                search_time=float(search_time);
                '''
                to get the time we use regular expression to get the first index where integer digit occur.
                From that index to the end of string contains only the time information.using some split and manipulation we get the
                opening & closing time

                '''
                '''
                 [0-9]  is the  regular expression to get the  index where integer digit occur.and start()is the function
                 whic returns the index of first occurance
                '''
                index = re.search("[0-9]", each).start();
                '''
                 Now from that index to the end will contain time information only
                 time_range ,open_time,close_time are the temporary variable
                '''
                each = each[index:];
                time_range = each.split("-");
                closing_time = 0.0;
                opening_time=0.0;
                close_time = time_range[1].split(" ");
                open_time = time_range[0].split(" ");
                if close_time[2] == "pm":
                    if re.search(":",close_time[1]) is  None:
                        closing_time = 12 + int(close_time[1]);
                    else:
                        closing_time=12+int(close_time[1].split(":")[0]);
                else:
                    if re.search(":",close_time[1]) is  None:
                        closing_time = int(close_time[1]);
                    else:
                        closing_time=int(close_time[1].split(":")[0]);

                if open_time[2] == "pm":
                    if re.search(":",open_time[0]) is  None:
                        opening_time = 12 + int(open_time[0]);
                    else:
                        opening_time=12+int(open_time[0].split(":")[0]);

                else:
                    if re.search("[:]",open_time[0]) is  None:
                        opening_time = int(open_time[0]);
                    else:
                        opening_time=int(open_time[0].split(":")[0]);
                print(len(close_time[1].split(":")[0]));
                if re.search(":",close_time[1]) is not None:
                    closing_time = str(closing_time) + "." + str(close_time[1].split(":")[1]);
                if re.search(":",open_time[0]) is not None:
                    opening_time = str(opening_time) + "." + str(open_time[0].split(":")[1]);
                opening_time=float(opening_time);
                closing_time = float(closing_time);
                if closing_time>=search_time and opening_time<=search_time:
                    time_found=1;
            '''
             Here if day_found and time_found both are 1 than we get our required inforamtion.that means this restaurant is available
             at the date & time given by the user.as mentioned the top of the function definiton names is the variable which
             contains all the restaurant available at the given time .And we add the name of the restaurant in the names list.
              And finnaly we can break the loop if we get information and go for the next string we split using the delimiter /.
             '''
            if day_found == 1 and time_found==1:
                names.append(values[0]);
                break
    return names;


#file_name is name of the file;
file_name = "/home/dev/Downloads/project-2/restaurant.csv";


#text is the input given by the user &below code is validating the input
text=[];
try:
    text.append(int(raw_input("enter date")));
    if text[0]<=0 or text[0] >30 :
        print "enter date only in between 1-30";
        sys.exit();
except ValueError:
    print "Enter only integer value"
    sys.exit()
try:
    text.append(int(raw_input("enter Month  ")))
    if  text[1]<=0 or text[1]>12 :
        print "enter Month only in between 1-12";
        sys.exit();
except ValueError:
    print "Enter only integer value"
    sys.exit()

try:
    text.append(int(raw_input("enter Year >=1900  ")))
    if  text[2]<1900  :
        print "enter Year only in between 1900 ";
        sys.exit();
except ValueError:
    print "Enter only integer value"
    sys.exit()
try:
    text.append(int(raw_input("enter hour  ")))
    if  text[3]<0 or text[3]>23 :
        print "enter Hour only in between 0-23";
        sys.exit();
except ValueError:
    print "Enter only integer value"
    sys.exit()
try:
    text.append(int(raw_input("enter Minute  ")))
    if  text[1]<0 or text[1]>59 :
        print "enter Minute only in between 0-59";
        sys.exit();
except ValueError:
    print "Enter only integer value"
    sys.exit()

'''
splitting the input  data to create date time object.
There may be a chance user will not give minutes in this case we include minutes as 0(zero).
'''

date = datetime.datetime(int(text[2]),int(text[1]),int(text[0]),int(text[3]),int(text[4]));

'''
names is list of resturant opens at given time and this accepts the returned list by 
find_open_restautrans() function which take file_name and datetime  object as argument        
'''

names = find_open_restaurants(file_name, date);
print(names);
